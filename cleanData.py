from trainPriceRaw import staList_SH, staList_ZJ, staList_JS, staName_SH, staName_ZJ, staName_JS
import json

# print(len(staName_SH), len(staList_SH))
# print(len(staName_ZJ), len(staList_ZJ))
# print(len(staName_JS), len(staList_JS))

staList = staList_SH + staList_ZJ + staList_JS
print(len(staList))
staName = staName_SH + staName_ZJ + staName_JS
print(len(staName))

result = {
    'allStation_to_allStation': []
}

for dep in staList:
    depStaResult = {
        'id': staName[staList.index(dep)][:-16],
        'eachStation_to_allStation_data': []
    }
    for arr in staList:
        arrStaResult = {
            'id': staName[staList.index(arr)][:-16],
        }

        f = open('data/tripsInfoRaw/{}-{}.json'.format(dep, arr), 'r')
        obj = json.loads(f.read())
        tripDataList = obj['data']
        # print(tripDataList)

        trainNumList = []
        startTimeList = []
        arriveTimeList = []
        timeNeededList = []
        priceList = []

        for tripDataItem in tripDataList:
            trainDataItem = tripDataItem['queryLeftNewDTO']

            if trainDataItem['from_station_telecode'] == dep and trainDataItem['to_station_telecode'] == arr:
                secSeatPrice = None if trainDataItem['ze_price'] == '--' else int(trainDataItem['ze_price']) / 10
                hardSeatPrice = None if trainDataItem['yz_price'] == '--' else int(trainDataItem['yz_price']) / 10
                hardBedPrice = None if trainDataItem['yw_price'] == '--' else int(trainDataItem['yw_price']) / 10
                noSeatPrice = None if trainDataItem['wz_price'] == '--' else int(trainDataItem['wz_price']) / 10
                ticketPrice = secSeatPrice or hardSeatPrice or hardBedPrice or noSeatPrice

                if ticketPrice is None:
                    print('>>> ' + trainDataItem['station_train_code'] + ' ticket price is None')
                    ticketPrice = -1

                print(trainDataItem['station_train_code'],
                      trainDataItem['from_station_name'],
                      dep,
                      '=>',
                      arr,
                      trainDataItem['to_station_name'],
                      ticketPrice)

                trainNumList.append(trainDataItem['station_train_code'])
                startTimeList.append(trainDataItem['start_time'])
                arriveTimeList.append(trainDataItem['arrive_time'])
                timeNeededList.append(trainDataItem['lishi'])
                priceList.append(ticketPrice)

        print('====================')
        arrStaResult['eachStation_to_eachStation_data'] = {
            'train_number': trainNumList,
            'start_time': startTimeList,
            'arrive_time': arriveTimeList,
            'time_needed': timeNeededList,
            'price': priceList
        }

        depStaResult['eachStation_to_allStation_data'].append(arrStaResult)
    result['allStation_to_allStation'].append(depStaResult)

jsonResult = json.dumps(result)
f = open('data/tripsInfo/all.json', 'w+')
f.write(jsonResult)
f.close()
