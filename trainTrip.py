import json

depSta = 'SXH'
arrSta = 'SZH'


def convertPastTimetoMin(pastTimeRaw):
    pastTimeSeg = pastTimeRaw.split(':')
    pastTime = int(pastTimeSeg[0]) * 60 + int(pastTimeSeg[1])
    return pastTime


f = open('data/tripsInfoRaw/{}-{}.json'.format('SXH', 'KAH'), 'r')
rawTripInfo = json.loads(f.read())
trainNumList = []
seatPriceList = []
pastTimeList = []
for rawTripItemInfo in rawTripInfo['data']:
    tripInfoBody = rawTripItemInfo['queryLeftNewDTO']
    if (tripInfoBody['from_station_telecode'] == depSta) and (tripInfoBody['to_station_telecode'] == arrSta):
        trainNum = tripInfoBody['station_train_code']
        secSeatPrice = None if tripInfoBody['ze_price'] == '--' else int(tripInfoBody['ze_price']) / 10
        hardSeatPrice = None if tripInfoBody['yz_price'] == '--' else int(tripInfoBody['yz_price']) / 10

        seatPrice = secSeatPrice or hardSeatPrice

        pastTime = convertPastTimetoMin(tripInfoBody['lishi'])

        print(trainNum, str(seatPrice), pastTime)

        trainNumList.append(trainNum)
        seatPriceList.append(seatPrice)
        pastTimeList.append(pastTime)

obj = {
    'trainNumList': trainNumList,
    'seatPriceList': seatPriceList,
    'pastTimeList': pastTimeList
}
f = open('data/tripsInfo/{}-{}.json'.format(depSta, arrSta), 'w+')
f.write(json.dumps(obj))
f.close()
