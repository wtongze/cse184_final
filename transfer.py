from trainPriceRaw import staList_SH, staList_ZJ, staList_JS, staName_SH, staName_ZJ, staName_JS
import json

f = open('data/tripsInfo/all.json', 'r')
data = json.loads(f.read())
f.close()

staList = staList_SH + staList_ZJ + staList_JS
staName = staName_SH + staName_ZJ + staName_JS

printSep = '=========================='




def getStaEdge(sta):
    edgeList = []
    for depData in data['allStation_to_allStation']:
        if depData['id'] == sta:
            arrDataRaw = depData['eachStation_to_allStation_data']
            for arrData in arrDataRaw:
                if len(arrData['eachStation_to_eachStation_data']['train_number']) > 0:
                    # print(len(arrData['eachStation_to_eachStation_data']['train_number']))
                    # print('append ' + arrData['id'])
                    edgeList.append(arrData['id'])
    return edgeList


def validateTransferInfo(dep, arr, stop1):
    # print(printSep)
    # print('Validating: {} => {} => {}'.format(dep, stop1, arr))
    trip1DataRaw = getTrainListBetween(dep, stop1)
    trip2DataRaw = getTrainListBetween(stop1, arr)
    trip1Data = trip1DataRaw['train_number']
    trip2Data = trip2DataRaw['train_number']
    # print('{} => {} has {} possible trains'.format(dep, stop1, len(trip1Data)))
    # print(trip1Data)
    transResult = {'possiblePathList': []}
    for i in range(len(trip1Data)):
        # print(trip1DataRaw['train_number'][i], trip1DataRaw['start_time'][i], trip1DataRaw['arrive_time'][i],
        #       trip1DataRaw['time_needed'][i], trip1DataRaw['price'][i])
        for j in range(len(trip2Data)):
            # print(trip2DataRaw['train_number'][j], trip2DataRaw['start_time'][j], trip2DataRaw['arrive_time'][j],
            #       trip2DataRaw['time_needed'][j], trip2DataRaw['price'][j])
            hour = int(trip1DataRaw['arrive_time'][i].split(':')[0]) <= int(trip2DataRaw['start_time'][j].split(':')[0])
            minute = int(trip1DataRaw['arrive_time'][i].split(':')[1]) < int(
                trip2DataRaw['start_time'][j].split(':')[1])

            start1 = int(trip1DataRaw['start_time'][i].split(':')[0])
            start2 = int(trip2DataRaw['start_time'][j].split(':')[0])
            end1 = int(trip1DataRaw['arrive_time'][i].split(':')[0])
            end2 = int(trip2DataRaw['arrive_time'][j].split(':')[0])

            check1 = start1 + end1 < 24 and start1 + end2 < 24 and start1 + start2 < 24
            check2 = start2 + end1 < 24 and start2 + end2 < 24 and end1 + end2 < 24

            if (hour is True) and (minute is True) and check1 and check2 and trip1DataRaw['train_number'][i] != \
                    trip2DataRaw['train_number'][j]:
                # print(trip1DataRaw['train_number'][i], trip1DataRaw['start_time'][i], trip1DataRaw['arrive_time'][i],
                #       trip1DataRaw['time_needed'][i], trip1DataRaw['price'][i])
                # print(trip2DataRaw['train_number'][j], trip2DataRaw['start_time'][j], trip2DataRaw['arrive_time'][j],
                #       trip2DataRaw['time_needed'][j], trip2DataRaw['price'][j])
                # print(trip1DataRaw['price'][i] + trip2DataRaw['price'][j])
                # print(calculateTimeDiff(trip2DataRaw['arrive_time'][j], trip1DataRaw['start_time'][i]))
                path = {
                    'dep': dep,
                    'stop1': stop1,
                    'arr': arr,
                    'train1num': trip1DataRaw['train_number'][i],
                    'train2num': trip2DataRaw['train_number'][j],
                    'train1start': trip1DataRaw['start_time'][i],
                    'train1end': trip1DataRaw['arrive_time'][i],
                    'train2start': trip2DataRaw['start_time'][j],
                    'train2end': trip2DataRaw['arrive_time'][j],
                    'totalTimeMin': calculateTimeDiff(trip2DataRaw['arrive_time'][j], trip1DataRaw['start_time'][i]),
                    'totalPrice': trip1DataRaw['price'][i] + trip2DataRaw['price'][j]
                }
                transResult['possiblePathList'].append(path)
    return transResult

    # print()
    # print('{} => {} has {} possible trains'.format(stop1, end, len(trip2Data)))
    # print(printSep)
    # print(trip2Data)


def getTrainListBetween(dep, arr):
    for depData in data['allStation_to_allStation']:
        if depData['id'] == dep:
            arrDataRaw = depData['eachStation_to_allStation_data']
            for arrData in arrDataRaw:
                if arrData['id'] == arr:
                    return arrData['eachStation_to_eachStation_data']


def calculateTimeDiff(time1, time2):
    minDiff = (int(time1.split(':')[0]) * 60 + int(time1.split(':')[1])) - (
                int(time2.split(':')[0]) * 60 + int(time2.split(':')[1]))
    if int(time1.split(':')[0]) < int(time2.split(':')[0]):
        minDiff += 24 * 60
    return minDiff


def getTransfer(start, end):
    depEdgeList = getStaEdge(start)
    print('{} can reach {} stations without transfer'.format(start, len(depEdgeList)))
    validStopList = []
    for possibleStop in depEdgeList:
        # print('{} - {}'.format(possibleStop, getStaEdge(possibleStop)))
        if end in getStaEdge(possibleStop):
            validStopList.append(possibleStop)
    # print(validStopList)
    print(
        'There are {} possible stops that can be used to reach {} from {} in 1 transfer'.format(len(validStopList), end,
                                                                                                start))

    result = []
    for potentialStop in validStopList:
        tempPartResult = validateTransferInfo(start, end, potentialStop)
        for path in tempPartResult['possiblePathList']:
            result.append(path)
    # getTransferInfo(start, end)
    priceFirst = sorted(result, key=lambda i: i['totalPrice'])
    timeFirst = sorted(result, key=lambda i: i['totalTimeMin'])

    print()
    print('{} ----> {} with 1 transfer'.format(start, end))
    print('====== Top {} Cheapest Transfer Route ======'.format(min(len(priceFirst), 5)))
    for i in range(min(len(priceFirst), 5)):
        tripData = priceFirst[i]
        hour = tripData['totalTimeMin'] // 60
        if hour < 10:
            hour = '0' + str(hour)
        minute = tripData['totalTimeMin'] % 60
        if minute < 10:
            minute = '0' + str(minute)
        print(
            '¥ {} / {} hr {} min : {} ({}) ===<{}>===> ({}) {} ({}) ===<{}>==> {} ({})'.format(tripData['totalPrice'],
                                                                                               hour,
                                                                                               minute, tripData['dep'],
                                                                                               tripData['train1start'],
                                                                                               tripData['train1num'],
                                                                                               tripData['train1end'],
                                                                                               tripData['stop1'],
                                                                                               tripData['train2start'],
                                                                                               tripData['train2num'],
                                                                                               tripData['arr'],
                                                                                               tripData['train2end']))
    print()
    print('====== Top 5 Fastest Transfer Route ======')
    for i in range(5):
        tripData = timeFirst[i]
        hour = tripData['totalTimeMin'] // 60
        if hour < 10:
            hour = '0' + str(hour)
        minute = tripData['totalTimeMin'] % 60
        if minute < 10:
            minute = '0' + str(minute)
        print('{} hr {} min / ¥ {} : {} ({}) ===<{}>===> ({}) {} ({}) ===<{}>==> {} ({})'.format(hour, minute,
                                                                                                 tripData['totalPrice'],
                                                                                                 tripData['dep'],
                                                                                                 tripData[
                                                                                                     'train1start'],
                                                                                                 tripData['train1num'],
                                                                                                 tripData['train1end'],
                                                                                                 tripData['stop1'],
                                                                                                 tripData[
                                                                                                     'train2start'],
                                                                                                 tripData['train2num'],
                                                                                                 tripData['arr'],
                                                                                                 tripData['train2end']))


depSta = 'Shanghai Hongqiao'
arrSta = 'Nanjing'
getTransfer(depSta, arrSta)
